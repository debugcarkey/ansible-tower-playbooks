## Configure AWS Provider
provider "aws" {}

## Create VPC if vpc_create is true
module "eks_vpc" {
    count                   =       var.vpc_create == true ? 1 : 0
    source                  =       "../../../../vendor/terraform/modules/vpc/"
    vpc_name                =       "eks" 
    vpc_description         =       "VPC for EKS clusters"
    vpc_tags                =       {"name" = "eks"}
    vpc_ipv4_cidr           =       "10.1.0.0/16"
}

## Create EKS Cluster if cluster_create is true
module "eks_cluster" {
    count                   =       var.cluster_create == true ? 1 : 0
    source                  =       "../../../../vendor/terraform/modules/eks/"
    cluster_name            =       var.cluster_name
    cluster_version         =       var.cluster_version
    # cluster_subnets         =       module.eks_vpc.vpc.subnets          
}
