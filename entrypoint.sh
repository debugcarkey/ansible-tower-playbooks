
pip install -r /app/requirements/pip.txt
ansible-galaxy install -r /app/requirements/ansible.yaml

# If you want to terminate the container after the job is completed, comment the line below.
python3 -m http.server
