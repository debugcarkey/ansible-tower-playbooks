 
#To Access docker container locally  docker exec -it $(docker ps --filter "name=search-ansible" | awk 'FNR==2 {print $1}') /bin/bash 

FROM bitnami/python:3.8 as baseimage
LABEL mantainer="platform@fuseuniversal.com"
LABEL description="Search Service Ansible Script"

# Install Kubectl
RUN apt-get update -y
RUN apt-get install -y gnupg2 

RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg |  apt-key add -
RUN touch /etc/apt/sources.list.d/kubernetes.list 
RUN echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" |  tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update -y
RUN apt-get install -y  kubectl jq ssh-client

# Install helm 
RUN curl https://get.helm.sh/helm-v3.8.0-linux-amd64.tar.gz  -o  /tmp/helm-v3.8.0-linux-amd64.tar.gz
RUN tar  -zxvf /tmp/helm-v3.8.0-linux-amd64.tar.gz
RUN mv linux-amd64/helm /usr/local/bin/helm

# Install anible and required dependenices.
RUN pip3 install ansible

# Install AWS CLI V2
RUN curl https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o /tmp/awscliv2.zip     
RUN unzip /tmp/awscliv2.zip -d /tmp
RUN /tmp/aws/install
RUN aws --version
RUN export AWS_PAGER=""

# Stage to execute docker container in development environment
FROM baseimage as run

FROM baseimage as build
COPY . /app
WORKDIR /app
RUN chmod +x /app/entrypoint.sh