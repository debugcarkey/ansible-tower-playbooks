### About Ansible Tower Playbooks
This repository contains playbooks that can be executed from ansible tower or AWX. You can test all the playbooks using ansible runner in dev environment before testing it in Ansible Tower or AWX.

Ansible Runner is a tool and python library that helps when interfacing with Ansible directly or as part of another system whether that be through a container image interface, as a standalone tool, or as a Python module that can be imported. 

### Make sure that the followig  tools are installed before running any playbooks locally
- Ansible Version >= 2.11.6
- Ansible Galaxy Version >= 2.11.6
- Git
- python3

### Install ansible and python dependencies 
ansible-galaxy install -r requirements/ansible.yaml
pip3 install -r requirements/pip.txt

### Configuration Process
- Create env/extravars based on env/extravars.sample
- Generate AWS Access Key Id and AWS Secret Access Key and use them in env/extravars  

### Playbooks
``` 
export ANSIBLE_PYTHON_INTERPRETER=$(which python3)

# Execute test using ansible runner. 
ansible-runner run  -p test.yaml --process-isolation --process-isolation-executable docker --container-image debugcarkey/ansible-automation-engine .

# Manage EKS Cluster
ansible-runner run  -p aws/eks/aws-eks.yaml --process-isolation --process-isolation-executable docker --container-image debugcarkey/ansible-automation-engine:latest .

# Setup AWX Minikube 
# Make sure that the minikube and kubectl is instaled. 
ansible-playbook project/awx/minikube/awx-minikube-setup.yaml -i inventory/hosts

```

### Setup AWX
-  Create Custom Execution Environment
![Create Custom Execution Environment](./docs/setup/create-custom-runner.png ".")

- Create New Project
![Create New Project](./docs/setup/create-new-project.png ".")

- Create New Job Template
![Create Job Template](./docs/setup/create-job-template.png ".")

### Troubleshoot 
- docker run -it  -v ${PWD}:/runner --env-file ./.env debugcarkey/ansible-automation-engine /bin/bash