output vpc {
  value = local.vpc[*]
}

output vpc_id{
  value = aws_vpc.vpc.id
}