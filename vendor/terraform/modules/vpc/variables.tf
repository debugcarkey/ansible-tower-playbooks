variable "vpc_name" {
    type = string
}

variable "vpc_description"{
    type = string
    default = "VPC"
}

variable "vpc_ipv4_cidr"{
    type = string
    default = "10.1.0.0/16"
}

variable "vpc_tags" {
    type = map
}

variable "dynamic_subnet_cidr_block" {
    type = string
    default = "10.1.%s.0/24"
}

# variable "vpc_security_groups" {
#     # type =  map(
#     #         # object({
#     #         #     name = string
#     #         #     description = string
#     #         #     inbound_rules = object({
#     #         #         type = string
#     #         #         protocol = string
#     #         #         port_range = string
#     #         #         source = list(string)
#     #         #     })
#     #         # })
#     # )
#     type = map
#     default = {}
# }