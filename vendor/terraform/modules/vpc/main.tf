data "aws_availability_zones" "available" {}

# Create AWS VPC
resource aws_vpc vpc {
  cidr_block           = var.vpc_ipv4_cidr
  enable_dns_hostnames = true

  tags = merge(var.vpc_tags,{
    Name      = format("%s-Vpc", var.vpc_name)
  })
}

# Create VPC subnet
resource "aws_subnet" "this" {
  count = 3
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  vpc_id     = aws_vpc.vpc.id
  cidr_block = format(var.dynamic_subnet_cidr_block, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = format("%s-subnet", var.vpc_name)
  }
}

# Create internet gateway for the VPC created 
resource aws_internet_gateway igw {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name      = format("%s-Igw", var.vpc_name)
  }
}

# Add routing table to Internet Gateway.
resource "aws_route_table" "eks" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "eks" {
  count = 2

  subnet_id      = aws_subnet.this.*.id[count.index]
  route_table_id = aws_route_table.eks.id
}

# Create Security Group for the VPC created.
# resource "aws_security_group" "security_group" {
#   for_each        =       var.vpc_security_groups
#   name            =       each.value.name
#   description     =       each.value.description
#   vpc_id          =       aws_vpc.vpc.id

#   tags = {
#       "Name" = format("%s", each.value.name)
#   }

#   dynamic "ingress" {
#     for_each = each.value.inbound_rules
#     content {
#       description = "description ${ingress.key}"
#       from_port   = ingress.value.port_range
#       to_port     = ingress.value.port_range
#       protocol    = "${ingress.value.protocol}"
#       cidr_blocks = "${ingress.value.source}"
#     }
#   }
# }

# Output vpc variable.  
locals {
  vpc = {
    name = var.vpc_name
    id = aws_vpc.vpc.id,
    //security_groups = { for k,v in aws_security_group.security_group : k => v.id }
    subnets = resource.aws_subnet.this.*.id
  }
}