variable "cluster_name"{
    type = string
}

variable "cluster_version"{
    type = string
}

variable "cluster_create"{
    type = bool
    default = true
}

variable "cluster_subnets"{
    type = list(string)
    default = []
}