# Step 1: Create AWS IAM role 
resource "aws_iam_role" "this" {
  name = "terraform-eks-${var.cluster_name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# #####
# # Step 2: Attach policy to role created in step 1.
resource "aws_iam_role_policy_attachment" "eks-cluster-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.this.name
}

# # Step 2: Attach policy to role created in step 1.
resource "aws_iam_role_policy_attachment" "eks-vpc-resource-controller-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.this.name
}
# #####

# # # Step 3: 
# # resource "aws_security_group" "${cluster_name}-cluster" {
# #   name        = "terraform-eks-${cluster_name}"
# #   description = "Cluster communication with worker nodes"
# #   vpc_id      = aws_vpc.eks.id

# #   egress {
# #     from_port   = 0
# #     to_port     = 0
# #     protocol    = "-1"
# #     cidr_blocks = ["0.0.0.0/0"]
# #   }

# #   tags = {
# #     Name = "terraform-eks-${cluster_name}"
# #   }
# # }

# # resource "aws_security_group_rule" "${cluster_name}-cluster-ingress-workstation-https" {
# #   cidr_blocks       = ["0.0.0.0/0"]
# #   description       = "Allow workstation to communicate with the cluster API Server"
# #   from_port         = 443
# #   protocol          = "tcp"
# #   security_group_id = aws_security_group.${cluster_name}-cluster.id
# #   to_port           = 443
# #   type              = "ingress"
# # }


# resource "aws_eks_cluster" "this" {
#   name     = var.cluster_name
#   version  = var.cluster_version
#   role_arn = aws_iam_role.this.arn

#   vpc_config {
#     //security_group_ids = [aws_security_group.${cluster_name}.id]
#     subnet_ids         = var.cluster_subnets //aws_subnet.eks[*].id
#   }

#   depends_on = [
#     aws_iam_role_policy_attachment.eks-cluster-policy,
#     aws_iam_role_policy_attachment.eks-vpc-resource-controller-policy
#   ]
# }
